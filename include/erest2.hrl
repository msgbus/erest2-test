%%%-------------------------------------------------------------------
%%% @author zhengyinyong
%%% @copyright (C) 2016, Yunba
%%% @doc
%%%
%%% @end
%%% Created : 04. 五月 2016 下午4:11
%%%-------------------------------------------------------------------
-author("zhengyinyong").

-include_lib("epublish/include/epublish.hrl").

%%--------------------------------------------------------------------
%% RESTful API
%%--------------------------------------------------------------------
-record(rs_state, {
    req_count = 0,
    start_time,
    protocol_version = 16#13,
    response_content,
    msg_queue_ttl = 3000
}).

-record(rest_args, {
    method,
    appkey,
    appid,
    seckey,
    topic,
    alias,
    message,
    need_trace,
    topic_query_type,
    opts = #opts{}
}).

%% RESTful API return code
-define(REST_OK, 0).

%% RESTful API error description
%% {ERROR_TYPE, ERROR_CODE, ERROR_DESCRIPTION}
-define(REST_ERROR_LISTS, [
    {invalid_parameter, 1, <<"invalid parameter">>},
    {internal_error, 2, <<"internal server error">>},
    {no_application, 3, <<"no application">>},
    {timeout, 4, <<"timeout">>},
    {alias_not_exist, 5, <<"alias not found">>},
    {permission_denied, 6, <<"permission denied">>},
    {missing_opts, 7, <<"missing opts">>},
    {invalid_secky, 8, <<"invalid seckey">>},
    {invalid_method, 9, <<"invalid method">>},
    {request_no_body, 10, <<"invalid parameter">>},
    {not_enough_parameters, 11, <<"invalid parameter">>},
    {alias_offline, 12, <<"alias offline">>},
    {query_topic_failed, 13, <<"internal server error">>},
    {unknown_topic_query_type, 14, <<"unknown topic query type">>},
    {unknown_error, -1, <<"internal server error">>}
]).

%% ETS Cache Lists
-define(APPID_TO_APPKEY, appid_to_appkey).
-define(APPKEY_TO_APPID, appkey_to_appid).
-define(APPKEY_TO_SECKEY, appkey_to_seckey).

-define(ETSCACHELISTS, [
    ?APPID_TO_APPKEY, ?APPKEY_TO_APPID, ?APPKEY_TO_SECKEY
]).

%% TODO: HTTP return code 需要进一步细化
%% HTTP return code
-define(HTTP_OK, 200).
-define(HTTP_INTERNAL_SERVER_ERROR, 500).

%% RESTful API method
-define(PUBLISH, publish).
-define(PUBLISH2, publish2).
-define(PUBLISH_TO_ALIAS, publish_to_alias).
-define(PUBLISH2_TO_ALIAS, publish2_to_alias).
-define(PUBLISH_TO_ALIAS_BATCH, publish_to_alias_batch).
-define(PUBLISH2_TO_ALIAS_BATCH, publish2_to_alias_batch).
-define(PUBLISH_ASYNC, publish_async).
-define(PUBLISH2_ASYNC, publish2_async).
-define(PUBLISH_CHECK, publish_check).
-define(QUERY_TOPIC_SIZE, query_topic_size).

-define(REST_PUBLISHER_UID, 0).

%% Route table
-record(erest2_route_table, {
    appid,
    flag,
    ip,
    port,
    tag,
    time
}).

-define(ONLINE, 1).
-define(OFFLINE, 0).

-define(PUBLISH_TO_ALIAS_PREFIX, <<",yta/">>).