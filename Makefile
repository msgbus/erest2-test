.PHONY: all deps clean release

all: compile

compile: deps
	./rebar -j8 compile

deps:
	./rebar -j8 get-deps

clean:
	./rebar -j8 clean

relclean:
	rm -rf rel/erest2

generate: compile
	cd rel && .././rebar -j8 generate

run: generate
	./rel/erest2/bin/erest2 start

console: generate
	./rel/erest2/bin/erest2 console

foreground: generate
	./rel/erest2/bin/erest2 foreground

erl: compile
	erl -pa ebin/ -pa deps/*/ebin/ -s erest2
