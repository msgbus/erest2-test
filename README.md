## Overview

本项目做了以下工作：

- 把之前项目中 [erest](https://bitbucket.org/msgbus/erest/overview) 中 RESTful API 逻辑独立成一个模块并加以重构；

- 使用 [epublish](https://bitbucket.org/msgbus/epublish/overview) 模块中的 `publish` API；

这样处理后 RESTful 模块能够变得更为独立和轻巧，方便部署和后期扩展需求。