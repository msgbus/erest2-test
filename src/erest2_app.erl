-module(erest2_app).

-behaviour(application).

-include("erest2.hrl").

%% Application callbacks
-export([start/2, stop/1]).

%% ===================================================================
%% Application callbacks
%% ===================================================================

start(_StartType, _StartArgs) ->
    start_cowboy(),
    start_mongodb(),
    init_ets_cache(),
    erest2_sup:start_link().

stop(_State) ->
    ok.

%%%===================================================================
%%% Internal functions
%%%===================================================================
start_cowboy() ->
    {ok, CowboyNBAcceptor} = application:get_env(erest2, cowboy_nb_acceptor),
    {ok, RestHttpPort} = application:get_env(erest2, rest_http_port),

    Dispatch = cowboy_router:compile([
        {'_', [
            {"/", erest2_handler, []},
            {"/rest/[...]", erest2_handler, []}
        ]}
    ]),
    {ok, _} = cowboy:start_http(erest2, CowboyNBAcceptor, [{port, RestHttpPort}], [
                               {env, [{dispatch, Dispatch}]}
    ]).

start_mongodb() ->
    ReplicaSet = erest2_utils:get_env(erest2, mongodb, replica_set),
    DBName = erest2_utils:get_env(erest2, mongodb, dbname),
    {ok, _Pid} = mongodberl:start_link({replset, {mongodbpool, {rel, ReplicaSet}, DBName, 5}}).

init_ets_cache() ->
    erest2_cache:init(?ETSCACHELISTS).