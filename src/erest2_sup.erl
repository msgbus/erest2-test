-module(erest2_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

%% Helper macro for declaring children of supervisor
-define(CHILD(I, Type), {I, {I, start_link, []}, permanent, 5000, Type, [I]}).
-define(SUP_CHILD(I, Args), {I, {I, start_link, Args}, permanent, infinity, supervisor, [I]}).

%% ===================================================================
%% API functions
%% ===================================================================

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

%% ===================================================================
%% Supervisor callbacks
%% ===================================================================
init([]) ->
    RestartStrategy = erest2_utils:get_env(erest2, general_supervisor, restart_strategy),
    MaxRestarts = erest2_utils:get_env(erest2, general_supervisor, max_restarts),
    MaxSecondsBetweenRestarts = erest2_utils:get_env(erest2, general_supervisor, max_seconds_between_restarts),

    SupFlags = {RestartStrategy, MaxRestarts, MaxSecondsBetweenRestarts},

    Process = [],

    {ok, {SupFlags, Process} }.
