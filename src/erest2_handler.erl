%%%-------------------------------------------------------------------
%%% @author zhengyinyong
%%% @copyright (C) 2016, Yunba
%%% @doc
%%%
%%% @end
%%% Created : 02. 六月 2016 下午5:19
%%%-------------------------------------------------------------------
-module(erest2_handler).
-author("zhengyinyong").

-compile({parse_transform, lager_transform}).

-include("erest2.hrl").

%% API
-export([init/3, rest_init/2, allowed_methods/2, content_types_provided/2, content_types_accepted/2]).
-export([respond/4, handle_get/2, handle_post/2]).

init({tcp, http}, _Req, _Opts) ->
    {upgrade, protocol, cowboy_rest}.

rest_init(Req, _Opts) ->
    {ok, Req, #rs_state{
        start_time = erest2_utils:timestamp_in_millisec(),
        msg_queue_ttl = erest2_utils:get_env(erest2, msg_queue_ttl)
    }}.

allowed_methods(Req, Context) ->
    {[<<"GET">>, <<"POST">>, <<"HEAD">>], Req, Context}.

content_types_provided(Req, Context) ->
    {[{<<"application/json">>, handle_get}], Req, Context}.

content_types_accepted(ReqData, Context) ->
    ReqestCount = Context#rs_state.req_count,
    Context2 = Context#rs_state{req_count = ReqestCount + 1},
    {[{<<"application/json">>, handle_post}], ReqData, Context2}.

handle_get(Req, Context) ->
    {Parameter, Req1} = cowboy_req:qs_vals(Req),
    Result = case format_parameter(get, Parameter) of
                 {ok, RestArgs} ->
                     do_rest(RestArgs, Req1, Context);
                 {error, Reason} ->
                     respond(?HTTP_INTERNAL_SERVER_ERROR, Reason, Req1, Context)
             end,
    {halt, Result, Context}.

handle_post(Req, Context) ->
    Result = case get_parameter(Req) of
                 {ok, RestArgs} ->
                     do_rest(RestArgs, Req, Context);
                 {error, Reason} ->
                     respond(?HTTP_INTERNAL_SERVER_ERROR, Reason, Req, Context)
             end,
    {halt, Result, Context}.

do_rest(RestArgs, Req, Context) ->
    case check_paramter(RestArgs) of
        {ok, NewRestArgs} ->
            case process_rest_method(NewRestArgs, Context) of
                {ok, Result} ->
                    respond(?HTTP_OK, ?REST_OK, Req, Context#rs_state{response_content = Result});
                {error, Reason} ->
                    respond(?HTTP_INTERNAL_SERVER_ERROR, Reason, Req, Context)
            end;
        {error, Reason} ->
            respond(?HTTP_INTERNAL_SERVER_ERROR, Reason, Req, Context)
    end.

%%%===================================================================
%%% Publish Method
%%%===================================================================
process_rest_method(RestArgs, Context) ->
    Method = erest2_utils:make_sure_atom(RestArgs#rest_args.method),
    case Method of
        ?PUBLISH ->
            publish_to_topic(RestArgs, Context);
        ?PUBLISH2 ->
            publish_to_topic(RestArgs, Context);
        ?PUBLISH_TO_ALIAS ->
            publish_to_alias(RestArgs, Context);
        ?PUBLISH2_TO_ALIAS ->
            publish_to_alias(RestArgs, Context);
        ?PUBLISH_TO_ALIAS_BATCH ->
            publish_to_alias_batch(RestArgs, Context);
        ?PUBLISH2_TO_ALIAS_BATCH ->
            publish_to_alias_batch(RestArgs, Context);
        ?PUBLISH_ASYNC->
            publish_async(RestArgs, Context);
        ?PUBLISH2_ASYNC ->
            publish_async(RestArgs, Context);
        ?PUBLISH_CHECK ->
            publish_check(RestArgs, Context);
        ?QUERY_TOPIC_SIZE ->
            query_topic_size(RestArgs, Context);
        _Else ->
            {error, invalid_method}
    end.

publish_to_topic(RestArgs, Context) ->
    MessageId = erest2_utils:generate_message_id(),
    MsgQueueTTL = case RestArgs#rest_args.opts#opts.time_to_live of
                      undefined ->
                          Context#rs_state.msg_queue_ttl;
                      TTL ->
                          TTL
                  end,
    PublishArgs = #epublish_args {
        type = publish_to_topic,
        uid =?REST_PUBLISHER_UID,
        message_id = MessageId,
        appid = RestArgs#rest_args.appid,
        topic = RestArgs#rest_args.topic,
        appkey = RestArgs#rest_args.appkey,
        protocol_version = Context#rs_state.protocol_version,
        payload = RestArgs#rest_args.message,
        msg_queue_ttl = MsgQueueTTL,
        qos = RestArgs#rest_args.opts#opts.qos,
        opts = RestArgs#rest_args.opts
    },
    case epublish:publish(PublishArgs) of
        ok ->
            {ok, [{message_id, MessageId}]};
        {error, Reason} ->
            {error, Reason}
    end.

publish_to_alias(RestArgs, Context) ->
    Alias = RestArgs#rest_args.alias,
    case Alias /= undefined of
        true ->
            do_publish_to_alias(Alias, RestArgs, Context);
        false ->
            {error, alias_undefined}
    end.

%% TODO: need to refractor
do_publish_to_alias(Alias, RestArgs, Context) ->
    Appkey = RestArgs#rest_args.appkey,
    MsgQueueTTL = case RestArgs#rest_args.opts#opts.time_to_live of
                      undefined ->
                          Context#rs_state.msg_queue_ttl;
                      TTL ->
                          TTL
                  end,
    case erest2_storage:get_uid_by_alias(Alias, Appkey) of
        {ok, Uid} ->
            case erest2_storage:get_route_table_item(Uid) of
                {ok, {Flag, _Ip, _Port, Tag}} ->
                    case Flag of
                        ?ONLINE ->
                            MessageId = erest2_utils:generate_message_id(),
                            {ok, Platform} = epublish_utils:make_sure_integer(erest2_storage:get_platform(Uid)),
                            PublishArgs = #epublish_args {
                                type = publish_to_one_uid,
                                message_id = MessageId,
                                appid = RestArgs#rest_args.appid,
                                appkey = RestArgs#rest_args.appkey,
                                protocol_version = Context#rs_state.protocol_version,
                                uid = Uid,
                                node_tag = Tag,
                                platform = Platform,
                                msg_queue_ttl = MsgQueueTTL,
                                payload = RestArgs#rest_args.message,
                                qos = RestArgs#rest_args.opts#opts.qos,
                                topic = iolist_to_binary([?PUBLISH_TO_ALIAS_PREFIX, erest2_utils:make_sure_binary(Alias)])
                            },
                            case epublish:publish(PublishArgs) of
                                ok ->
                                    {ok, [{message_id, MessageId}]};
                                {error, Reason} ->
                                    {error, Reason}
                            end;
                        ?OFFLINE ->
                            {error, alias_offline}
                    end;
                _Else ->
                    {error, get_route_table_error}
            end;
        _Else ->
            {error, alias_not_exist}
    end.

%% TODO: 此处实现跟原来 erest 不一样，这里是同步 publish，需要更改为 mget 方式
publish_to_alias_batch(RestArgs, Context) ->
    AliasLists = RestArgs#rest_args.alias,
    case (AliasLists /= undefined) andalso (is_list(AliasLists))of
        true ->
            Result = [do_publish_to_alias(Alias, RestArgs, Context) || Alias <- AliasLists],
            case lists:keyfind(error, 1, Result) of
                {error, _Error} ->
                    {error, internal_error};
                false ->
                    lists:keyfind(ok, 1, Result)
            end;
        false ->
            {error, invalid_parameter}
    end.

publish_async(RestArgs, Context) ->
    publish_to_topic(RestArgs, Context).

publish_check(_RestArgs, _Context) ->
    ok.

query_topic_size(RestArgs, _Context) ->
    TopicQueryType = RestArgs#rest_args.topic_query_type,
    Topic = RestArgs#rest_args.topic,
    Appkey = RestArgs#rest_args.appkey,
    Result = case TopicQueryType of
                 <<"online">> ->
                     query_topic(online, Appkey, Topic);
                 <<"offline">> ->
                     query_topic(offline, Appkey, Topic);
                 <<"total">> ->
                     query_topic(total, Appkey, Topic);
                 _Else ->
                     {error, unknown_topic_query_type}
    end,
    Result.

%%FIXME: {error, Reason} => Reason 需要打印在日志上
query_topic(Type, Appkey, Topic) ->
    {ok, EtopicfsGetURL} = application:get_env(erest2, etopicfs_get_url),
    URL = EtopicfsGetURL ++ erest2_utils:make_sure_string(Appkey) ++ "/" 
                         ++ erest2_utils:make_sure_string(Topic)++ "/?type=" ++ erest2_utils:make_sure_string(Type),
    
    case erest2_utils:http_get(URL) of
        {ok, Body} ->
            {ok, [{num, erest2_utils:make_sure_integer(jiffy:decode(Body))}]};
        {error, _Reason} ->
            {error, query_topic_failed}
    end.

%%%===================================================================
%%% Internal Function
%%%===================================================================
get_parameter(Req) ->
    case cowboy_req:has_body(Req) of
        true ->
            case cowboy_req:body(Req) of
                {ok, Body, _Req1} ->
                    parse_parameter(Body);
                {error, Error} ->
                    {error, Error}
            end;
        false ->
            {error, request_no_body}
    end.

parse_parameter(JSONBody) ->
    try jiffy:decode(JSONBody) of
        {Props} ->
            format_parameter(post, Props);
        _ ->
            {error, invalid_parameters}
    catch
        _Type:Error ->
            {error, Error}
    end.

format_parameter(Type, Props) ->
    Appkey = erest2_utils:get_key_value(<<"appkey">>, Props),
    SecKey = erest2_utils:get_key_value(<<"seckey">>, Props),
    Message = erest2_utils:get_key_value(<<"msg">>, Props),
    Method = erest2_utils:get_key_value(<<"method">>, Props),
    Topic = erest2_utils:get_key_value(<<"topic">>, Props),
    Alias = erest2_utils:get_key_value(<<"alias">>, Props),
    NeedTrace = erest2_utils:get_key_value(<<"trace">>, Props, false),
    Opts = erest2_utils:get_key_value(<<"opts">>, Props),
    TopicQueryType = erest2_utils:get_key_value(<<"topic_query_type">>, Props),

    FormatOpts = case (Opts /= undefined) and (is_tuple(Opts)) and (Type =:= post) of
                     true ->
                         {Opts1} = Opts,
                         generate_opts(Opts1);
                     false ->
                         #opts{}
                 end,

    case {Appkey /= undefined, SecKey /= undefined, Method /= undefined, (Topic /= undefined) or (Alias /= undefined)} of
        {true, true, true, true} ->
            {ok, #rest_args {
                appkey = Appkey,
                seckey = SecKey,
                message = Message,
                method = Method,
                topic = Topic,
                alias = Alias,
                need_trace = NeedTrace,
                topic_query_type = TopicQueryType,
                opts = FormatOpts
            }};
        _Else ->
            {error, not_enough_parameters}
    end.

check_paramter(RestArgs) ->
    Appkey = RestArgs#rest_args.appkey,
    Seckey = RestArgs#rest_args.seckey,
    case check_secret_key(Appkey, Seckey) of
        true ->
            case appkey_to_appid(Appkey) of
                {ok, Appid} ->
                    {ok, RestArgs#rest_args{appid = Appid}};
                {error, _Reason} ->
                    {error, no_application}
            end;
        false ->
            {error, invalid_seckey}
    end.

check_secret_key(Appkey, SecKey) ->
    SecKeyBin = erest2_utils:make_sure_binary(SecKey),
    case erest2_cache:get(?APPKEY_TO_SECKEY, Appkey) of
        none ->
            case mongodberl:get_value_from_mongo(mongodbpool, seckey, Appkey) of
                {true, Value} ->
                    erest2_cache:set(?APPKEY_TO_SECKEY, Appkey, Value),
                    case Value of
                        SecKeyBin -> true;
                        _ -> false
                    end;
                _Else ->
                    false
            end;
        CachedSecKey ->
            case CachedSecKey of
                SecKeyBin -> true;
                _ -> false
            end
    end.

respond(HTTPReturnCode, Type, Req, Context) ->
    Body = generate_response_body(Type, Context),
    {ok, Req1} = cowboy_req:reply(HTTPReturnCode, [{<<"content-type">>, <<"application/json">>}], Body, Req),
    StartTime = Context#rs_state.start_time,
    RequestTime = (erest2_utils:timestamp_in_millisec() - StartTime) / 1000,
    lager:info("Rest API request time ~p seconds", [RequestTime]),
    Req1.

generate_response_body(Type, Context) ->
    case Type of
        ?REST_OK ->
            ReponseContent = [{erest2_utils:make_sure_binary(K), erest2_utils:make_sure_binary(V)} || {K, V} <- Context#rs_state.response_content],
            jiffy:encode({[{<<"status">>, ?REST_OK}] ++ ReponseContent});
        _ ->
            {_ErrorType, ErrorCode, ErrorDescription} = case lists:keyfind(Type, 1, ?REST_ERROR_LISTS) of
                                                            false ->
                                                                lists:keyfind(unknown_error, 1, ?REST_ERROR_LISTS);
                                                            {ErrorType1, ErrorCode1, ErrorDescription1} ->
                                                                {ErrorType1, ErrorCode1, ErrorDescription1}
                                                        end,
            jiffy:encode({[{<<"status">>, ErrorCode}, {<<"error">>, ErrorDescription}]})
    end.

appkey_to_appid(Appkey) ->
    case erest2_cache:get(?APPKEY_TO_APPID, Appkey) of
        none ->
            case erest2_storage:get_appid_from_redis(Appkey) of
                {ok, Appid} ->
                    erest2_cache:set(?APPKEY_TO_APPID, Appkey, Appid),
                    {ok, Appid};
                {error, Reason} ->
                    {error, Reason}
            end;
        Appid ->
            {ok, Appid}
    end.

generate_opts(Opts) ->
    TimeToLive = erest2_utils:get_key_value(<<"time_to_live">>, Opts),
    PlatForm = erest2_utils:get_key_value(<<"platform">>, Opts),
    TimeDelay = erest2_utils:get_key_value(<<"time_delay">>, Opts),
    Location = erest2_utils:get_key_value(<<"location">>, Opts),
    QoS = erest2_utils:get_key_value(<<"qos">>, Opts, 1),
    APNJson = erest2_utils:get_key_value(<<"apn_json">>, Opts),

    FormatApnJson = case APNJson /= undefined of
                  true ->
                      jiffy:encode(APNJson);
                  false ->
                      undefined
              end,

    Opts1 = #opts {
        time_to_live = TimeToLive,
        platform = PlatForm,
        time_delay = TimeDelay,
        location = Location,
        qos = QoS,
        apn_json = FormatApnJson
    },
    Opts1.
