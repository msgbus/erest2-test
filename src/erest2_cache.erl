%%%-------------------------------------------------------------------
%%% @author zhengyinyong
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 26. 六月 2016 上午11:19
%%%-------------------------------------------------------------------
-module(erest2_cache).
-author("zhengyinyong").

%% API
-export([init/1, get/2, set/3]).

init(EtsCacheLists) ->
    [ets:new(EtsCache, [set, public, named_table]) || EtsCache <- EtsCacheLists].

get(EtsCache, Key) ->
    case ets:lookup(EtsCache, Key) of
        [] ->
            none;
        [{Key, Value}] ->
            Value
    end.

set(EtsCache, Key, Value) ->
    ets:insert(EtsCache, {Key, Value}).
