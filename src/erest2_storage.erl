%%%-------------------------------------------------------------------
%%% @author zhengyinyong
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 26. 六月 2016 上午11:50
%%%-------------------------------------------------------------------
-module(erest2_storage).
-author("zhengyinyong").

-include("erest2.hrl").

%% API
-export([get_appid_from_redis/1, get_route_table_item/1, get_uid_by_alias/2, get_platform/1]).

get_route_table_item(Uid) ->
    case erest2_redis:read_record(redis_route_table_pool, Uid, erest2_route_table) of
        {ok, Item}  when Item#erest2_route_table.flag =/= undefined ->
            #erest2_route_table{flag = Flag, ip = Ip, port = Port, tag = Tag} = Item,
            {ok, {Flag, Ip, Port, Tag}};
        _ ->
            {error, key_enoent}
    end.

get_uid_by_alias(Alias, Appkey) ->
    Key = iolist_to_binary([erest2_utils:make_sure_binary(Appkey), <<"-">>, erest2_utils:make_sure_binary(Alias)]),
    case cbclient_storage:get(alias, Key, raw_binary) of
        {ok, Uid} ->
            {ok, Uid};
        Error ->
            Error
    end.

get_appid_from_redis(Appkey) ->
    Key = iolist_to_binary([<<"cidpre-">>, erest2_utils:make_sure_binary(Appkey)]),
    case erest2_redis:get_cache(
        redis_reginfo_pool,
        Key,
        fun(_Key) ->
            case mongodberl:get_value_from_mongo(mongodbpool, incressid, Appkey) of
                {true, Value} ->
                    Appid = integer_to_list(Value),
                    AppidPrefix = "0000000000" ++ Appid,
                    AppidPrefix2 = string:sub_string(AppidPrefix, string:len(AppidPrefix) - 9),
                    {ok, list_to_binary(AppidPrefix2)};
                _Else ->
                    {error, mongodb_unavailable}
            end
        end) of
        {ok, Appid} ->
            {ok, integer_to_list(binary_to_integer(Appid))};
        not_found ->
            {error, key_enoent};
        {error, Reason} ->
            {error, Reason}
    end.

get_platform(Uid) ->
    UidBin = epublish_utils:make_sure_binary(Uid),
    case erest2_redis:get_cache(
        redis_reginfo_pool,
        <<UidBin/binary, "-p">>,
        fun(_Key) ->
            case get_platform_from_webservice(Uid) of
                {ok, undefined} ->
                    not_found;
                {ok, P} ->
                    {ok, P};
                E ->
                    E
            end
        end
    ) of
        {ok, Platform} ->
            {ok, erest2_utils:make_sure_integer(Platform)};
        not_found ->
            {error, key_enoent};
        {error, Reason} ->
            {error, Reason}
    end.

get_platform_from_webservice(Uid) ->
    RegURL = erest2_utils:get_env(erest2, reg_service_url),
    RequestURL = lists:append("device/platform/?uid=", erest2_utils:make_sure_string(Uid)),
    case erest2_utils:http_get(lists:append(RegURL, RequestURL)) of
        {ok, Body} ->
            case jiffy:decode(Body) of
                {Props} ->
                    Platform = proplists:get_value(<<"platform">>, Props),
                    {ok, Platform};
                _ ->
                    {error, <<"invalied parameters">>}
            end;
        Other ->
            Other
    end.