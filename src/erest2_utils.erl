%%%-------------------------------------------------------------------
%%% @author zhengyinyong
%%% @copyright (C) 2016, Yunba
%%% @doc
%%%
%%% @end
%%% Created : 06. 五月 2016 上午10:34
%%%-------------------------------------------------------------------
-module(erest2_utils).
-author("zhengyinyong").

-include("erest2.hrl").

-compile({parse_transform, lager_transform}).

%% API
-export([make_sure_binary/1, make_sure_string/1, make_sure_integer/1, make_sure_atom/1]).
-export([verify_appkey/1, get_env/3, timestamp_in_millisec/0, get_key_value/2, get_key_value/3, generate_message_id/0,
    http_get/1, http_get/2, get_env/2]).

-define(HTTP_TIMEOUT, 10000). %% timeout values are in milliseconds.

verify_appkey(_EpublishMeta) ->
    ok.

make_sure_binary(Data) ->
    if
        is_list(Data) ->
            list_to_binary(Data);
        is_integer(Data) ->
            integer_to_binary(Data);
        is_atom(Data) ->
            atom_to_binary(Data, latin1);
        true ->
            Data
    end.

make_sure_string(Data) ->
    if
        is_binary(Data) ->
            binary_to_list(Data);
        is_integer(Data) ->
            integer_to_list(Data);
        is_atom(Data) ->
            atom_to_list(Data);
    true ->
        Data
    end.

make_sure_integer(Data) ->
    if
        is_binary(Data) ->
            binary_to_integer(Data);
        is_list(Data) ->
            list_to_integer(Data);
        is_integer(Data) ->
            Data;
        true ->
            Data
    end.

make_sure_atom(Data) ->
    if
        is_binary(Data) ->
            binary_to_atom(Data, utf8);
        is_list(Data) ->
            list_to_atom(Data);
        is_atom(Data) ->
            Data;
        true ->
            Data
    end.

get_env(Applicaiton, Module, Arg) ->
    case application:get_env(Applicaiton, Module) of
        {ok, ArgsLists} ->
            case proplists:get_value(Arg, ArgsLists) of
                undefined ->
                    undefined;
                Value ->
                    Value
            end
    end.

get_env(Application, Module) ->
    case application:get_env(Application, Module) of
        {ok, Args} ->
            Args;
        _ ->
            undefined
    end.

timestamp_in_millisec() ->
    {Mega, Sec, Micro} = os:timestamp(),
    (Mega*1000000 + Sec)*1000 + round(Micro/1000).

get_key_value(Key, List) ->
    case lists:keyfind(Key, 1, List) of
        false ->
            undefined;
        {_K, Value} ->
            Value
    end.

get_key_value(Key, List, Default) ->
    case lists:keyfind(Key, 1, List) of
        false ->
            Default;
        {_K, Value} ->
            Value
    end.

generate_message_id() ->
    MessageIdBin = snowflake:new(),
    <<MessageId:64/integer>> = MessageIdBin,
    MessageId.

http_get(URL) ->
    http_get(URL, ?HTTP_TIMEOUT).

http_get(URL, Timeout) ->
    case http_get_request(URL, Timeout) of
        {error, connection_closing} ->
            http_get_request(URL, Timeout);
        Else ->
            Else
    end.

%%%===================================================================
%%% Internal functions
%%%===================================================================
http_get_request(URL, Timeout) ->
    try ibrowse:send_req(URL,[{"accept", "application/json"}],get, [], [], Timeout) of
        {ok, ReturnCode, _Headers, Body} ->
            case ReturnCode of
                "200" ->
                    {ok, Body};
                _Other ->
                    {error, Body}
            end;
        {error, connection_closing} ->
            {error, connection_closing};
        {error, REASON} ->
            {error, REASON}
    catch
        _Type:Error ->
            {error, Error}
    end.
